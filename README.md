# Cortext Method: Sashimi

This cortext-method provides the methodology introduced in [Domain-topic models with chained dimensions: Charting an emergent domain of a major oncology conference](https://doi.org/10.1002/asi.24606) employing the [sashimi python library](https://gitlab.com/solstag/sashimi).
