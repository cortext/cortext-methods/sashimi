# cortext_sashimi - SASHIMI for Cortext
#
# Author(s):
# * Ale Abdo <abdo@member.fsf.org>
#
# License:
# [GNU-GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
#
# Reference repository:
# <https://gitlab.com/cortext/cortext-methods/sashimi>
#
# Contributions are welcome, get in touch with the author(s).

from .method import Method

__all__ = ["Method"]
