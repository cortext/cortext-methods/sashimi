#!/usr/bin/env python
# coding: utf-8

from cortextlib.legacy import containerize  # noqa
from cortextlib.main import main

if __name__ == "__main__":
    main()
