# cortext_sashimi - SASHIMI for Cortext
#
# Author(s):
# * Ale Abdo <abdo@member.fsf.org>
#
# License:
# [GNU-GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
#
# Reference repository:
# <https://gitlab.com/cortext/cortext-methods/sashimi>
#
# Contributions are welcome, get in touch with the author(s).

import re
import shutil

import yaml

try:
    from sashimi import GraphModels
    from sashimi.ioio import ioio
    from sashimi.corpus.nlp import wordsentence
except Exception:
    from abstractology.src.sashimi import GraphModels
    from abstractology.src.sashimi.ioio import ioio
    from abstractology.src.sashimi.corpus.nlp import wordsentence

from cortextlib import Analysis, CortextMethod, Job


class Method(CortextMethod):
    """
    This class implements the SASHIMI family of methods.
    Cortext Documentation: <https://docs.cortext.net/sashimi/>
    """

    def __init__(self, job: Job):
        super().__init__(job)
        params = self.params
        self.method = self.job.method_name

        if self.method == "prepare_corpus":
            n_grams = params["n_grams"]
            params["n_grams"] = 1 if n_grams == "No" else int(n_grams)
            transform_map = {"none": False, "discard frequency": "set"}
            params["transform"] = transform_map[params["transform"]]
            params.setdefault("text_sources", [])
            params.setdefault("token_sources", [])
        else:
            source_name_map = {
                # (method, kind): source
                ("domain_model", "domain-topic"): "prepared_corpus",
                ("domain_model", "domain-chained"): "domain_topic",
                ("domain_maps", "domain-topic"): "domain_topic",
                ("domain_maps", "domain-chained"): "domain_chained",
            }
            source_name = source_name_map[self.method, params["kind"]]
            source_param = params[f"loaded_{source_name}"]
            params["source_analysis"] = {
                source_name: self._get_config_path(source_param)
            }
            for value in {*source_name_map.values()}:
                params.pop(f"loaded_{value}", None)

    def __exit__(self, exc_type, exc_value, traceback):
        super().__exit__(exc_type, exc_value, traceback)

    def main(self, **params):
        getattr(self, self.method)(**params)

    def _get_config_path(self, raw_analysis_id):
        match = re.fullmatch(r".*\((.*)\)", raw_analysis_id)
        if match is None:
            return None
        analysis_id = match.group(1)
        analysis_dir = Analysis(self.job.project_id, analysis_id).dir
        self.job.logger.info(f"Loaded config: {analysis_dir}")
        return analysis_dir / "config.json"

    def prepare_corpus(self, text_sources, token_sources, transform, n_grams=None):
        self.job.progress.set_total(4)

        data, _ = self.data.to_dataframe(columns=text_sources + token_sources)
        corpus = GraphModels(
            storage_dir=self.job.dir, column="_sashimi_tokens", load_data=False
        )
        corpus.load_data(data, name=self.data.db.path.name)
        self.job.progress.advance()

        corpus.text_sources = text_sources
        corpus.token_sources = token_sources
        # use wordsentence instead of default, for deeper nesting; may be unecessary
        corpus.process_sources(
            ngrams=n_grams, language="en", tokens_to_doc=wordsentence
        )
        self.job.progress.advance()

        if transform:
            corpus.full_data = corpus.transform_corpus(trans=transform)
            corpus.column += "_" + transform
        self.job.progress.advance()

        corpus.store_data(columns=[corpus.column])
        corpus.register_config(self.job.dir / "config.json")
        update_meta(self.job, "sashimi", f"{self.job.name} ({self.job.analysis_id})")
        self.job.progress.advance()

    def domain_model(
        self, kind, source_analysis, dimension=None, domain_selection=None
    ):
        self.job.progress.set_total(3)

        is_domain_topic = kind == "domain-topic"
        config_path = source_analysis[
            "prepared_corpus" if is_domain_topic else "domain_topic"
        ]
        columns = [] if is_domain_topic else [dimension]
        corpus = load_graphology(
            config_path=config_path, job_data=self.data, data_columns=columns
        )
        self.job.progress.advance()

        if is_domain_topic:
            corpus.load_domain_topic_model()
            model_path = corpus.blocks_dir / corpus.blocks_name
            meta_key = "sashimi_dtm"
        else:
            corpus.set_chain(prop=dimension, matcher=None)
            if domain_selection:
                sel = corpus.domain_labels_to_selection(domain_selection)
                corpus.set_sample(sel)
            corpus.load_domain_chained_model()
            model_path = corpus.chained_dir / corpus.chained_name
            meta_key = "sashimi_chained"
        self.job.progress.advance()

        corpus.register_config(self.job.dir / "config.json")
        update_meta(self.job, meta_key, f"{self.job.name} ({self.job.analysis_id})")
        shutil.copy(model_path, self.job.dir)
        summary = get_summary(corpus)
        (self.job.dir / f'{summary["title"]}.txt').write_text(summary["contents"])
        self.job.progress.advance()

    def domain_maps(
        self,
        kind,
        source_analysis,
        title_column,
        time,
        url,
        url_template,
        domain_selection,
    ):
        self.job.progress.set_total(6)

        is_domain_topic = kind == "domain-topic"
        config_path = source_analysis[
            "domain_topic" if is_domain_topic else "domain_chained"
        ]
        columns = [title_column, time, url]
        corpus = load_graphology(
            config_path=config_path,
            job_data=self.data,
            data_columns=columns,
            col_title=title_column,
            col_time=time,
            col_url=url,
            url_template=url_template,
        )
        self.job.progress.advance()

        map_title = f"db: {self.job.db.path.name}, model: {corpus.blocks_name}"
        if is_domain_topic:
            corpus.load_domain_topic_model()
            map_title += f", entropy: {corpus.state.entropy()}"
            map_dir = corpus.blocks_adir
        else:
            corpus.load_domain_chained_model()
            map_title += f", chained: {corpus.graph_extend['prop']}"
            map_title += f", entropy: {corpus.state.entropy()}"
            map_dir = corpus.chained_adir
        self.job.progress.advance()

        # Selection
        if domain_selection:
            sel = corpus.domain_labels_to_selection(domain_selection)
            corpus.set_sample(sel)
        map_path = map_dir / corpus.domain_map(
            title=map_title, chained=False if is_domain_topic else True
        )
        self.job.progress.advance()
        shutil.copy(map_path, self.job.dir)
        self.job.progress.advance()

        # Tables
        if (dlevel := min(3, max(corpus.dblocks_levels))) > 1:
            btype = "ter" if is_domain_topic else "ext"
            report_paths = corpus.subxblocks_tables("doc", dlevel, None, btype)
            (report_dir := self.job.dir / "tables").mkdir(exist_ok=True)
            for report_path in report_paths:
                shutil.copy(report_path, report_dir)
        self.job.progress.advance()

        # Networks
        network_dir = self.job.dir / "networks"
        network_dir.mkdir(exist_ok=True)
        for dlevel in (dl for dl in corpus.dblocks_levels if dl < 4):
            for ter_ext_lvls in [(1, None)] if is_domain_topic else [(None, 1), (1, 1)]:
                network_spe_paths = [
                    *corpus.domain_network(
                        dlevel, *ter_ext_lvls, edges="specific"
                    ).values()
                ]
                for network_spe_path in network_spe_paths:
                    net_spe_dir = (
                        network_dir
                        / network_spe_path.suffix[1:]
                        # / "links:specific_to_domain"
                        # / f"level {dlevel}"
                    )
                    net_spe_dir.mkdir(parents=True, exist_ok=True)
                    shutil.copy(network_spe_path, net_spe_dir)
                if dlevel > 1:
                    network_com_paths = [
                        *corpus.domain_network(
                            dlevel, *ter_ext_lvls, edges="common"
                        ).values()
                    ]
                    for network_com_path in network_com_paths:
                        net_com_dir = (
                            network_dir
                            / network_com_path.suffix[1:]
                            # / "links:common_to_subdomains"
                            # / f"level {dlevel}"
                        )
                        net_com_dir.mkdir(parents=True, exist_ok=True)
                        shutil.copy(network_com_path, net_com_dir)
        self.job.progress.advance()

    def further_optimize(self):
        pass


def load_graphology(
    config_path,
    job_data,
    data_columns,
    col_title=None,
    col_time=None,
    col_url=None,
    url_template="{}",
):
    corpus = GraphModels(config_path=config_path, load_data=False)
    if corpus.graph_extend:
        data_columns.append(corpus.graph_extend["prop"])
    data, _ = job_data.to_dataframe(columns=data_columns)
    corpus.load_data(data, name=job_data.db.path.name)
    corpus.full_data = corpus.data.join(
        ioio.load_pandas(corpus.data_dir / corpus.data_name), how="right"
    )
    corpus._data_selection = slice(None)
    if col_title:
        corpus.col_title = col_title
    if col_time:
        corpus.col_time = col_time
    if col_url:
        corpus.col_url = f"_sashimi_url__{col_url}"
        corpus.full_data[corpus.col_url] = corpus.data[col_url].transform(
            lambda x: url_template.format(x)
        )
    return corpus


def update_meta(job, key, value):
    """
    Used to make previous analysis ids available as parameter choices in Cortext Manager.
    """
    job.db._metadata_update_root(lambda meta: meta.setdefault(key, []).append(value))


def get_summary(corpus):
    """
    Return a summary of the block hierarchy.
    :: dict(title: str , contents: yaml_str )
    """

    def get_bnums(blocks, blocks_levels):
        return [len(blocks[level].unique()) for level in blocks_levels]

    contents = {
        "documents": len(corpus.dblocks),
        "document blocks": get_bnums(corpus.dblocks, corpus.dblocks_levels),
        "terms": len(corpus.tblocks),
        "term blocks": get_bnums(corpus.tblocks, corpus.tblocks_levels),
        "chained": len(corpus.eblocks),
        "chained blocks": get_bnums(corpus.eblocks, corpus.eblocks_levels),
        "entropy": corpus.state.entropy(),
    }
    title = (
        f'entropy {contents["entropy"]} -'
        f' {contents["document blocks"][0]} document blocks'
    )
    if contents["chained"]:
        title = f'{title} - {contents["chained blocks"][0]} chained blocks'
    if contents["terms"]:
        title = f'{title} - {contents["term blocks"][0]} term blocks'
    return {"title": title, "contents": yaml.dump(contents, sort_keys=False)}
